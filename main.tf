provider "aws" {
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  region     = "us-west-1"
}



module "ec2_cluster" {
  source                 = "terraform-aws-modules/ec2-instance/aws"
  version                = "~> 2.0"

  name                   = "lunch-n-learn-cluster"
  ##
  instance_count         = 5
  ##
  ami                    = data.aws_ami.centos.image_id
  instance_type          = "t3a.medium"
  monitoring             = true
  vpc_security_group_ids = ["sg-a2ad2cd7"]
  subnet_id              = "subnet-12379e74"

  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}




data "aws_ami" "centos" {
  most_recent = true
  owners      = ["125523088429"]

  filter {
    name   = "name"
    values = ["CentOS 7*"]
  }

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }
}